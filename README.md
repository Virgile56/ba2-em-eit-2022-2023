# Projet BA2 EM-ELEC (Template)

Il s'agit d'un template pour créer votre dépôt sur le gitlab du projet BA2. Il suffit de "*forker*" ce projet vers un autre projet de votre groupe *EMELECx*.

![forker](Documentation/img/fork.png)

Un dépôt sert principalement au stockage de fichiers *textes*, **le stockage de fichiers *binaires* (vidéo, grosses images, etc.) devra donc être fortement limité**.

En pratique, la structure proposée, sous forme de dossiers, est la suivante :

- `Arduino` contient les codes de votre micro-contrôleur ainsi que les librairies : il faudra changer, dans les préférences de l'Arduino IDE, l'emplacement du carnet de croquis par ce dossier.
- `Documentation` contient l'ensemble de votre documentation interne (vos recherches, vos références (liens), vos tests, etc.)
- `Gestion` contient l'ensemble des vos fichiers relatifs à la gestion de projet : "status reports" en *Markdown*, "risk and issues logs" en *Excel* et votre fichier *GanttProject* mis à jour régulièrement.

Les documents (*readme, status reports*...) utilisent la syntaxe [Markdown](https://www.markdownguide.org/cheat-sheet/), qui permet une mise en forme simple du texte affiché

**N'hésitez pas modifier ce template par rapport à vos besoins !**
